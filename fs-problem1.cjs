const fs = require('fs');
const path = require('path');

function randomGeneratedData() {
  let data = {
    random: Math.random(),
  };
  return JSON.stringify(data, null, 2);
}

function createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, callback) {
  fs.mkdir(absolutePathOfRandomDirectory, (err) => {
    if (err) {
      return callback(err);
    }
    let fileNumbers = Array(randomNumberOfFiles).fill().map((_, index) => index + 1);
    let filesCreated = 0;

    function handleCreateFile(err) {
      if (err) {
        return callback(err);
      }

      filesCreated++;
      if (filesCreated === randomNumberOfFiles) {
        callback(null);
      }
    }

    return fileNumbers.map((file) => {
      let filename = `file${file}.json`;
      let filepath = path.join(absolutePathOfRandomDirectory, filename);
      let fileData = randomGeneratedData();

      fs.writeFile(filepath, fileData, handleCreateFile);
    });
  });
}

function deleteFile(directoryPath, callback) {
  fs.readdir(directoryPath, (err, files) => {
    if (err) {
      return callback(err);
    }

    if (files.length === 0) {
      return callback(null);
    }

    let deletedFile = 0;

    function handleDeleteFile(err) {
      if (err) {
        return callback(err);
      }

      deletedFile++;
      if (deletedFile === files.length) {
        callback(null);
      } else {
        deleteNextFile();
      }
    }

    function deleteNextFile() {
      const file = files[deletedFile];
      const filePath = path.join(directoryPath, file);

      fs.unlink(filePath, handleDeleteFile);
    }

    deleteNextFile();
  });
}

function fsproblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
  createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (err) => {
    if (err) {
      console.error("Error creating", err);
    } else {
      console.log("Created random json files", absolutePathOfRandomDirectory);
      deleteFile(absolutePathOfRandomDirectory, (err) => {
        if (err) {
          console.log("Error occurred in deleting files", err);
        } else {
          console.log("Files are deleted", absolutePathOfRandomDirectory);
        }
      });
    }
  });
}

module.exports = fsproblem1;
