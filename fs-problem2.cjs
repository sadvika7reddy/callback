const fs= require('fs')

function fsproblem2(filePath){
    fs.readFile(filePath,(err,data)=>{
        if(err){
            console.error(err)
        }
        else{
            let dataFile=data.toString().toUpperCase()
            let newFile = "newFile.txt"
           fs.writeFile(newFile,dataFile,(err)=>{
            if(err){
                console.error(`Error occured in the ${err}`)
            }
            else{
                 let filePath="fileNames.txt"
                fs.appendFile(filePath,newFile+'\n',(err)=>{
                    if(err){
                        console.error(`when appending the file ${err}`)
                    }
                    else{
                        //console.log("file uploaded in the fileName")
                        fs.readFile(newFile,(err,data)=>{
                            if(err){
                                console.error(`when reading the file ${err}`)
                            }
                            else{
                                let lowercaseData=data.toString().toLowerCase()
                                const sentences = printSentences(lowercaseData);
                                let newFile2='newFile2.rxt'
                                fs.writeFile(newFile2,sentences,(err)=>{
                                    if(err){
                                        console.error(`when writing the file ${err}`)
                                    }
                                    else{
                                        fs.appendFile(filePath,newFile2+'\n',(err)=>{
                                            if(err){
                                                console.error(`when appending the file ${err}`)
                                            }
                                            else{
                                                
                                                let sort=sortSentences(sentences)
                                                let newFile3='newFile3.txt'
                                                fs.writeFile(newFile3,sort,(err)=>{
                                                    if(err){
                                                        console.error(`when writing the file${err}`)
                                                    }
                                                    else{
                                                        fs.appendFile(filePath,newFile2+'\n',(err)=>{
                                                            if(err){
                                                                console.error(`when newfile3 appending ${err}`)
                                                            }
                                                            else{
                                                                fs.readFile(filePath,(err,data)=>{
                                                                    if(err){
                                                                        console.error(`when reading the content ${err}`)
                                                                    }
                                                                    else{
                                                                        fs.writeFile(filePath, '', (err) => {
                                                                            if (err) {
                                                                              console.error('Error deleting file content:', err);
                                                                            } else {
                                                                              console.log('File content deleted successfully.');
                                                                            }
                                                                          })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })

                            }
                        })


                    }

                })

            }
           })
        }

    })
}
function printSentences(content) {
    const sentences = content.match(/[^\.!\?]+[\.!\?]+/g);
    return sentences.map(sentence => sentence.trim());
  }

  function sortSentences(sentences) {
    
  
    return sentences.sort((a, b) => {
      const sentenceA = a.toLowerCase();
      const sentenceB = b.toLowerCase();
      return sentenceA.localeCompare(sentenceB);
    });
  
    
  }

module.exports=fsproblem2